import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/layout/header/header.component';
import { SidebarComponent } from './components/layout/sidebar/sidebar.component';
import { FooterComponent } from './components/layout/footer/footer.component';
import { LogoComponent } from './components/layout/header/partials/logo/logo.component';
import { NavigationComponent } from './components/layout/header/partials/navigation/navigation.component';
import { ControlsComponent } from './components/layout/header/partials/controls/controls.component';
import { HomeComponent } from './components/home/home.component';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      {path: '', component: HomeComponent, pathMatch: 'full'}
    ])
  ],
  declarations: [
    HeaderComponent, 
    SidebarComponent, 
    FooterComponent, 
    LogoComponent, 
    NavigationComponent, 
    ControlsComponent, 
    HomeComponent
  ],
  exports: [
    RouterModule,
    HeaderComponent,
    SidebarComponent,
    FooterComponent
  ]
})
export class CoreModule { }
