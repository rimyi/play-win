import { Team } from "./team";

export interface League {
    name:string;
    playersNum:number;
    players:Team[];
    private:boolean;
    managedBy:string;//admin, players
    admins:string[];
    shouldHaveScore:boolean;
    shouldRecalculate:boolean;
    password?:string;
    privateKey?:string;
    logo?:string;
    desciption?:string;
}
