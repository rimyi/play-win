export interface Team {
    name:string;
    description?:string;
    logo?:string;
    privateKey?:string;
    email?:string;
}
