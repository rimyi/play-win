import { TestBed, inject } from '@angular/core/testing';

import { AutosaveService } from './autosave.service';

describe('AutosaveService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AutosaveService]
    });
  });

  it('should be created', inject([AutosaveService], (service: AutosaveService) => {
    expect(service).toBeTruthy();
  }));
});
