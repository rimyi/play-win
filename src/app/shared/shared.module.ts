import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthService } from './services/auth/auth.service';
import { FiredbService } from './services/firedb/firedb.service';
import { LocalstorageService } from './services/localstorage/localstorage.service';
import { UserService } from './services/user/user.service';
import { AutosaveService } from './services/autosave/autosave.service';

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [
    CommonModule
  ],
  providers: [
    AuthService,
    FiredbService,
    LocalstorageService,
    UserService,
    AutosaveService
  ],
  declarations: []
})
export class SharedModule { }
